#include "Dataset.h"
#include "MeanShift.h"
#include "Timer.h"
#include <iostream>
#include <cmath>

void printMat(float *M, int nRows, int nCols=1) {
    for (int i = 0; i < nRows; ++i) {
        std::cout << i  << " | ";
        for (int j = 0; j < nCols; ++j) {
            std::cout << M[i * nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

void printMat(int *M, int nRows, int nCols=1) {
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            std::cout << M[i * nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

bool areEquals(const float *lhs, const float *rhs, int nRows, int nCols = 1) {
    float eps = 1e-7;
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            if ( std::abs(lhs[i * nCols + j] - rhs[i * nCols + j]) >= 0.005)
                return false;
        }
    }
    return true;
}

int main() {
    //std::string filename = "../data/data_100.csv";
    //std::string filename = "../data/data_1000.csv";
    std::string filename = "../data/data_3000.csv";
//    std::string filename = "../data/data_10000.csv";

    constexpr int nRows{3000};   // REMEMBER TO CHANGE THIS ONE !!!
    constexpr int nCols{3};
    
    std::cout << nRows << '\n';

    constexpr float threshold{1e+7};
    constexpr float bandwidth{2};
    constexpr float tolerance{1e-7};
    constexpr int maxIterations{100};
    constexpr float eps{0.05};

    auto *dataset = new Dataset(filename.c_str(), nRows, nCols);

    Timer t;
    t.start();
    float *shifted_data_seq = MeanShift(dataset->getData(), nRows, nCols, threshold, bandwidth, tolerance, maxIterations);
    float *clusters_seq = makeClusters(shifted_data_seq, nRows, nCols, eps);
    t.stop();
    double timeSeq = t.getElapsedTimeInMilliSec();
    std::cout << "Elapsed time:\t" << timeSeq << " [ms].\n";

    t.start();
    float *shifted_data_par = MeanShiftPar(dataset->getData(), nRows, nCols, threshold, bandwidth, tolerance, maxIterations);
    float *clusters_par = makeClusters(shifted_data_par, nRows, nCols, eps);
    t.stop();
    double timePar = t.getElapsedTimeInMilliSec();
    std::cout << "Elapsed time:\t" << timePar << " [ms].\n";

    std::cout << "SpeedUp:\t" << timeSeq / timePar << " X\n";

    areEquals(clusters_seq, clusters_par, nRows) ? std::cout << "OK.\n" : std::cout << "Not OK.\n";

    auto *clusters_dataset = new Dataset(clusters_par, nRows, 1);
    clusters_dataset->writeToCSV( (filename.substr(0, filename.size() - 4) + "_clusters.csv").c_str() );

    return 0;
}
