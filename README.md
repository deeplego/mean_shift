### Speedups vs number of data points  
    - GPU NVIDIA 1070 with MAX-Q Design  
    - CPU i7-8750H 6 cores (12 threads with hyperthreading)  
<img src="/presentation/figures/speedups_with_CUDA_kernel.png" width="750" height="300" title="speedups_vs_nDataPoints">

### Speedups vs number of cores  
    - hyperthreading deactivate (6 cores, 6 threads)
<img src="/presentation/figures/speedups_vs_nCores.png" width="750" height="300" title="speedups_vs_nCores">