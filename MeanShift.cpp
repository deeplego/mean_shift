//
// Created by lore on 4/16/19.
//

#include "MeanShift.h"

float *makeClusters(float *result, int nRows, int nCols, float eps) {
    auto *clusters = new float[nRows];
    bool *marks = new bool[nRows];
    float nCluster = 0;

    for (int i = 0; i < nRows; ++i) { marks[i] = false; }

    for (int i = 0; i < nRows; ++i) {
        if (!marks[i]) {
            marks[i] = true;
            clusters[i] = nCluster;

            std::cout << "centroid #" << nCluster << ": ";
            for (int j = 0; j < nCols; ++j) {
                std::cout << result[i * nCols + j] << ' ';
            }
            std::cout << '\n';

            for (int k = i + 1; k < nRows; ++k) {
                bool flag = true;
                for (int j = 0; j < nCols; ++j) {
                    if (std::abs(result[i * nCols + j] - result[k * nCols + j]) >= eps) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    marks[k] = true;
                    clusters[k] = nCluster;
                }
            }
            ++nCluster;
        }
    }
    delete[] marks;

    return clusters;
}

float *initCopy(const float *values, const int nRows, const int nCols) {
    auto *valuesCopy = new float[nRows * nCols];
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            valuesCopy[i * nCols + j] = values[i * nCols + j];
        }
    }
    return valuesCopy;
}

float *
MeanShift(float *values, int nRows, int nCols, float threshold, float bandwidth, float tolerance, int maxIterations) {
    auto *copy = initCopy(values, nRows, nCols);
    auto *numerator = new float[nCols];
    auto *denominator = new float[nCols];
    auto *error = new float[nCols];
    bool isTolerable{true};
    unsigned int counter{0};


    for (int ii = 0; ii < nRows; ++ii) {
        counter = 0;

        do {
            // clearing numerator and denominator
            for (int j = 0; j < nCols; ++j) { numerator[j] = 0; }
            for (int j = 0; j < nCols; ++j) { denominator[j] = 0; }

            float dist{0};
            float weight{0};

            // computing the distances from ii obs (copy) and all other obs (values)
            for (int i = 0; i < nRows; ++i) {
                dist = 0;
                weight = 0;
                // computing euclidean distance from ii obs (copy) and i obs (values)
                for (int j = 0; j < nCols; ++j) {
                    dist += (copy[ii * nCols + j] - values[i * nCols + j]) *
                            (copy[ii * nCols + j] - values[i * nCols + j]);
                }
                if (dist < threshold) {
                    weight += std::exp(-0.5 * ( dist / (bandwidth * bandwidth) ));
                    for (int j = 0; j < nCols; ++j) {
                        numerator[j] += weight * values[i * nCols + j];
                        denominator[j] += weight;
                    }
                }
            }

            isTolerable = true;
            for (int j = 0; j < nCols; ++j) {
                error[j] = copy[ii * nCols + j] - numerator[j] / denominator[j];
                error[j] = error[j] > 0 ? error[j] : -error[j];
                if (error[j] > tolerance) {
                    isTolerable = false;
                }
                copy[ii * nCols + j] = copy[ii * nCols + j] - error[j];
                copy[ii * nCols + j] = numerator[j] / denominator[j];
            }

            ++counter;
        } while (!isTolerable && counter < maxIterations);
    }

    delete[] error;
    delete[] denominator;
    delete[] numerator;

    return copy;
}

float *
MeanShiftPar(float *values, int nRows, int nCols, float threshold, float bandwidth, float tolerance, int maxIterations) {
    auto *copy = initCopy(values, nRows, nCols);

#pragma omp parallel for schedule(nonmonotonic:dynamic, 1) // 35
    for (int ii = 0; ii < nRows; ++ii) {

        auto *numerator = new float[nCols];
        auto *denominator = new float[nCols];
        auto *error = new float[nCols];
        bool isTolerable{true};
        unsigned int counter{0};

        do {
            // clearing numerator and denominator
            for (int j = 0; j < nCols; ++j) { numerator[j] = 0; }
            for (int j = 0; j < nCols; ++j) { denominator[j] = 0; }

            float dist{0};
            float weight{0};

            // computing the distances from ii obs (copy) and all other obs (values)
            for (int i = 0; i < nRows; ++i) {
                dist = 0;
                weight = 0;
                // computing euclidean distance from ii obs (copy) and i obs (values)
                for (int j = 0; j < nCols; ++j) {
                    dist += (copy[ii * nCols + j] - values[i * nCols + j]) *
                            (copy[ii * nCols + j] - values[i * nCols + j]);
                }
                if (dist < threshold) {
                    weight += std::exp(-0.5 * ( dist / (bandwidth * bandwidth) ));
                    for (int j = 0; j < nCols; ++j) {
                        numerator[j] += weight * values[i * nCols + j];
                        denominator[j] += weight;
                    }
                }
            }

            isTolerable = true;
            for (int j = 0; j < nCols; ++j) {
                error[j] = copy[ii * nCols + j] - numerator[j] / denominator[j];
                error[j] = error[j] > 0 ? error[j] : -error[j];
                if (error[j] > tolerance) {
                    isTolerable = false;
                }
                copy[ii * nCols + j] = copy[ii * nCols + j] - error[j];
                copy[ii * nCols + j] = numerator[j] / denominator[j];
            }

            ++counter;
        } while (!isTolerable && counter < maxIterations);

        delete[] error;
        delete[] denominator;
        delete[] numerator;
    }

    return copy;
}
