//
// Created by lore on 4/16/19.
//

#ifndef MEAN_SHIFT_MEANSHIFT_H
#define MEAN_SHIFT_MEANSHIFT_H

#include <iostream>
#include <cmath>
#include <omp.h>

float *makeClusters(float *result, int nRows, int nCols, float eps);

float *MeanShift(float *values, int nRows, int nCols, float threshold=10, float bandwidth=1, float tolerance=1e-7,
        int maxIterations=300);

float *MeanShiftPar(float *values, int nRows, int nCols, float threshold=10, float bandwidth=1, float tolerance=1e-7,
        int maxIterations=300);

#endif //MEAN_SHIFT_MEANSHIFT_H
