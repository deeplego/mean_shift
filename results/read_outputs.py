import os
import re
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument(
    "--executable",
    "-e",
    type=str,
    default="../cmake-build-debug/mean_shift",
)
#parser.add_argument("--schedule_type", "-s", type=str, default="dynamicV2")
#parser.add_argument("--size", "-n", type=int, default=100)
#parser.add_argument("--core_id", "-c", type=int, default=2)
flags = parser.parse_args()

n_experiments = 10
for i in range(n_experiments):
    txt = os.popen(f"./{flags.executable}").read()
    first_line = re.match(".*\n", txt).group()[:-1]

    results = re.findall("\d+.\d+", txt)
    filename = "./res-{0}-CUDA.csv".format(first_line)
    with open(filename, "a") as f:
        f.write(results[0])
        for i in range(1, len(results)):
            f.write(",")
            f.write(results[i])
        f.write("\n")
        
print(os.popen(f"wc -l {filename}").read())
